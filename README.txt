ABOUT PANELATOR

The panelator module is the easiest way to present an entire site with panels.
Panelator provides a standard template which uses 3-column layout, and adds a
"Current page contents" panel-pane in the central column.
Panel variants allow for different layouts and varying blocks in different
sections of the site.



WHAT'S THE DIFFERENCE BETWEEN PANELS EVERYWHERE AND PANELATOR?

Panels Everywhere is designed to take over the entire page - i.e. provide
header, menu-bar, footer, etc.  Panelator is designed to replace the page's
content section (and left/right sidebars).  It works with your existing theme.



INSTALLATION STEPS

1.  Copy the module to the appropriate modules directory.
    e.g. sites/all/modules
2.  Enable the module.
3.  Review the default template at admin/build/pages/edit/site_template.



DEPENDENCIES

1.  Ctools/page manager.
2.  Panels.



CUSTOMISATION

// TODO: add FAQs and common scenarios here.



STANDARD CONFIGURATION

Panelator provides an admin page at admin/build/panels/settings/panelator where
some settings can be changed:
- Use on admin pages
- Override other panel pages
- Provide a default page variant (i.e. panels template) for Panelator

These options correspond to the following variables:

$conf['panelator_use_on_admin_pages']
Set to TRUE to allow panelator to take over admin pages (i.e. pages whose URL
begins with "admin/").  Defaults to FALSE.

$conf['panelator_override_other_panel_pages']
Set to TRUE to allow panelator to take over pages which are already managed by
page manager.  Defaults to FALSE.

$conf['panelator_provide_default_site_template']
Set to FALSE to disable the default site-template variant provided by
panelator.  Defaults to TRUE.



ADVANCED CONFIGURATION

These variables do not have an admin page where they can be configured.  These
variables can be set in settings.php, and should only be set if required to do
so by another module.

$conf['panelator_site_template_disabled']
Set to TRUE to enable panelator, or FALSE to disable.  This is usually managed
automatically by the page manager module.  Defaults to TRUE.

$conf['panelator_use_hook_panelator_enabled_router_paths_alter']
This hook allows other modules to control which pages may be controlled by 
Panelator.
Set to TRUE to make panelator call hook_panelator_enabled_router_paths_alter.
By default, this hook will not fire.

$conf['panelator_delay_page_content_rendering']
Set to TRUE to delay invocation of the page callback until the page content
panel-pane is displayed (or the page manager task fails to find a validating
variant).  Note that if the "current page has content" selection criteria is
used, this option will have no effect.

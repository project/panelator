<?php

/**
 * @file
 * The default site-template provided by panelator.
 */

/**
 * Implementation of hook_default_page_manager_handlers().
 */
function panelator_default_page_manager_handlers() {
  if (variable_get('panelator_provide_default_site_template', TRUE)) {
    $handler = new stdClass;
    $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
    $handler->api_version = 1;
    $handler->name = 'site_template_panel_context';
    $handler->task = 'site_template';
    $handler->subtask = '';
    $handler->handler = 'panel_context';
    $handler->weight = 1;
    $handler->conf = array(
      'title' => 'Site template',
      'no_blocks' => 1,
      'pipeline' => 'standard',
      'css_id' => '',
      'css' => '',
      'contexts' => array(),
      'relationships' => array(),
      'access' => array(
        'plugins' => array(),
        'logic' => 'and',
      ),
    );
    $display = new panels_display;
    $display->layout = 'site_template';
    $display->layout_settings = array();
    $display->panel_settings = array(
      'style_settings' => array(
        'default' => NULL,
        'left' => NULL,
        'middle' => NULL,
        'right' => NULL,
      ),
    );
    $display->cache = array();
    $display->title = '';
    $display->content = array();
    $display->panels = array();
      $pane = new stdClass;
      $pane->pid = 'new-1';
      $pane->panel = 'middle';
      $pane->type = 'current_page_content';
      $pane->subtype = 'current_page_content';
      $pane->shown = TRUE;
      $pane->access = array();
      $pane->configuration = array(
        'context' => 'argument_current_page_content_1',
        'override_title' => 0,
        'override_title_text' => '',
      );
      $pane->cache = array();
      $pane->style = array(
        'settings' => NULL,
      );
      $pane->css = array();
      $pane->extras = array();
      $pane->position = 0;
      $display->content['new-1'] = $pane;
      $display->panels['middle'][0] = 'new-1';
    $display->hide_title = PANELS_TITLE_FIXED;
    $display->title_pane = '0';
    $handler->conf['display'] = $display;

    $handlers[$handler->name] = $handler;

    return $handlers;
  }
}

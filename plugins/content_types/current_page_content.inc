<?php

/**
 * @file
 * Plugin to handle the 'page_content' content type which allows the
 * regular page contents to be embedded into a panel.
 */

$plugin = array(
  'title' => t('Current page content'),
  'single' => TRUE,
  'icon' => 'icon_page.png',
  'description' => t('Page content that is available if the panel is being used to wrap content with the Panelator module.'),
  'category' => t('Page elements'),
  'required context' => new ctools_context_required(t('Page content'), 'current_page_content'),
);

/**
 * Output function for the 'page_content' content type.
 *
 * Outputs the mission statement for the site.
 */
function panelator_current_page_content_content_type_render($subtype, $conf, $panel_args, $context) {
  if (empty($context)) {
    return;
  }

  /**
   * Panelator can be configured to delay page-rendering until this pane is
   * displayed.  This may be used to improve performance on pages where an
   * alternative pane is providing the information required.  The setting is
   * controlled by the variable 'panelator_delay_page_content_rendering'.
   *
   * If set, the context will not generate the page content and the 'processed'
   * property will be FALSE.
   */
  if (!$context->processed) {
    if ($context->include_file) {
      require_once($context->include_file);
    }
    // The callback and arguments parameters are added by the context-handler.
    // These are the page-callback and page-arguments for the original menu
    // hook.
    $context->data = call_user_func_array($context->callback, $context->arguments);
  }

  if (empty($context->data)) {
    return;
  }

  if (is_string($context->data)) {
    $block = new stdClass();
    $block->content = $context->data;
    return $block;
  }
}

/**
 * Returns an edit form for custom type settings.
 */
function panelator_current_page_content_content_type_edit_form(&$form, &$form_state) {
  // provide a blank form so we have a place to have context setting.
}

/**
 * Admin info to show when administering the panel contents.
 */
function panelator_current_page_content_content_type_admin_info($subtype, $conf, $context) {
  $block->title = t('Main page content');
  $block->content = t('This will contain the content of page being viewed.');
  return $block;
}

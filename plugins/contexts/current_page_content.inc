<?php

/**
 * @file
 *
 * Plugin to provide a current_page_content context
 */

$plugin = array(
  'title' => t('Page content'),
  'description' => t('The page content.'),
  'context' => 'panelator_context_create_current_page_content',
  'keyword' => 'current_page_content',
  'no ui' => TRUE,
  'context name' => 'current_page_content',
);

/**
 * It's important to remember that $conf is optional here, because contexts
 * are not always created from the UI.
 */
function panelator_context_create_current_page_content($empty, $data = NULL, $conf = FALSE) {
  // The input is expected to be an array providing the original hook_menu's
  // callback, include file, and the arguments that were provided by the menu
  // handler.

  $context = new ctools_context('current_page_content');
  $context->plugin = 'current_page_content';

  if ($empty) {
    return $context;
  }

  if ($data !== FALSE ) {
    $callback = array_shift($data);
    $include_file = array_shift($data);
    if ($include_file) {
      require_once($include_file);
    }

    $context->callback = $callback;
    $context->include_file = $include_file;
    $context->arguments = $data;
    $context->title = t('Page content');
    if (variable_get('panelator_delay_page_content_rendering', FALSE)) {
      $context->processed = FALSE;
      $context->data = '';
    }
    else {
      $context->processed = TRUE;
      $context->data = call_user_func_array($callback, $data);
    }

    return $context;
  }
}

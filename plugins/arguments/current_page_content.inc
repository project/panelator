<?php

/**
 * @file
 *
 * Plugin to provide an argument handler for a raw current_page_content.
 * This has no UI and requires the original hook_menu parameters to be passed.
 */

$plugin = array(
  'title' => t("Current page content"),
  // keyword to use for %substitution
  'keyword' => 'current_page_content',
  'description' => t('A hidden argument that can be used to get the current page content as a context.'),
  'context' => 'panelator_current_page_content_context',

  'placeholder form' => array(
    '#type' => 'fieldset',
    'content' => array(
      '#title' => t('Page content'),
      '#type' => 'textarea',
      '#default_value' => '<p>' . t('Dummy page content') . '</p>',
    ),
  ),
  'no ui' => TRUE,
);

/**
 * Discover if this argument gives us the term we crave.
 */
function panelator_current_page_content_context($arg = NULL, $conf = NULL, $empty = FALSE) {
  // If unset it wants a generic, unfilled context.
  if ($empty) {
    return ctools_context_create_empty('current_page_content');
  }

  // Args should be an array providing the original hook_menu's callback,
  // include file, and the arguments that were provided by the menu handler.
  $context = ctools_context_create('current_page_content', $arg);
  $context->original_argument = $arg;

  return $context;
}

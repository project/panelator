<?php

/**
 * @file
 * Plugin to provide access control/visibility based on whether the current page context has data.
 * JS callbacks typically would not provide content.
 */

$plugin = array(
  'title' => t("Page content exists"),
  'description' => t('Control access by whether the current page returned content.'),
  'callback' => 'ctools_current_page_content_ctools_access_check',
  'settings form' => 'ctools_current_page_content_ctools_access_settings',
  'required context' => new ctools_context_required(t('Current page context'), 'current_page_content'),
  'defaults' => array('require_content' => TRUE),
);

/**
 * Settings form
 */
function ctools_current_page_content_ctools_access_settings(&$form, &$form_state, $conf) {
  $form['settings']['require_content'] = array(
    '#type' => 'radios',
    '#title' => t('Require content'),
    '#options' => array(
      0 => t('No'),
      1 => t('Yes'),
    ),
    '#default_value' => $conf['operator'],
    '#description' => t('Require the page content to contain content.'),
  );
}

/**
 * Check for access
 */
function ctools_current_page_content_ctools_access_check($conf, &$context) {
  if (empty($context)) {
    return FALSE;
  }
  elseif (!$context->processed) {
    if ($context->include_file) {
      require_once($context->include_file);
    }
    $context->data = call_user_func_array($context->callback, $context->arguments);
    $context->processed = TRUE;
  }
  return (bool) !empty($context->data);
}

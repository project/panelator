<?php
/**
 * @file
 * Page manager task plugin.
 */

/**
 * The site_template task is a panel page which can display any regular site
 * page within a panel.
 */
function panelator_site_template_page_manager_tasks() {
  return array(
    // This is a 'page' task and will fall under the page admin UI
    'task type' => 'page',
    'title' => t('Default page layout (Panelator)'),

    'admin title' => t('Default page layout (Panelator)'),
    'admin description' => t('When enabled, the site template allows all pages to be displayed in the panel page.'),
    'admin path' => FALSE,

    // Menu hooks to override the default page-callbacks and point to site_template instead.
    'hook menu' => 'panelator_site_template_menu',
    'hook menu alter' => 'panelator_site_template_menu_alter',

    // This task uses 'context' handlers and must implement these to give the
    // handler data it needs.
    'handler type' => 'context',
    'get arguments' => 'panelator_site_template_get_arguments',
    'get context placeholders' => 'panelator_site_template_get_contexts',

    // Allow this to be enabled or disabled:
    'disabled' => variable_get('panelator_site_template_disabled', TRUE),
    'enable callback' => 'panelator_site_template_enable',

    'tab title' => t('Edit site template'),
  );
}

/**
 * Ctools task handler for hook_menu_alter().
 *
 * Override the page-callback for every page (except pages which are already
 * managed by panels, or pages which are excluded).
 */
function panelator_site_template_menu_alter(&$items, $task) {
  if (variable_get('panelator_site_template_disabled', TRUE)) {
    return;
  }

  // Check which router-paths are enabled for panelator.
  $router_paths_enabled = _panelator_use_panelator($items);

  // Override every menu callback to use panelator.
  foreach ($items as $key => &$item) {
    // Check if this menu-router path should be handled by panelator.
    if (!$router_paths_enabled[$key]) {
      continue;
    }
    // Some menu items don't have a page callback (they inherit from their
    // parent). Don't alter these.
    elseif (!isset($item['page callback'])) {
      continue;
    }
    
    // Add the original callback and (if set) the include file, to the list of
    // page arguments, so the data is available when panelator handles the
    // menu-item.
    if (!array_key_exists('page arguments', $item)) {
      $item['page arguments'] = array();
    }
    $include_file = NULL;
    if ($item['file']) {
      $file_path = $item['file path'] ? $item['file path'] : drupal_get_path('module', $item['module']);
      $include_file = $file_path .'/'. $item['file'];
    }
    array_unshift($item['page arguments'], $item['page callback'], $include_file);

    // Override the menu entry's defaults.
    $item['page callback'] = 'panelator_site_template';
    $item['file path'] = $task['path'];
    $item['file'] = $task['file'];
  }
  unset($item);
}

/**
 * Menu callback to handle the site_template task.
 */
function panelator_site_template() {
  // Load my task plugin
  $task = page_manager_get_task('site_template');

  // Add the URL and actual page contents into contexts.
  ctools_include('context');
  ctools_include('context-task-handler');

  // This task requires 2 contexts: the URL and the current_page_content.
  $args = array(
    $_GET['q'],
    func_get_args(),
  );
  $contexts = ctools_context_handler_get_task_contexts($task, '', $args);
  $output = ctools_context_handler_render($task, '', $contexts, $args);
  if ($output !== FALSE) {
    return $output;
  }

  // Fallback to the default handler.  Check (in the context) if the callback
  // has already been processed.  Callbacks which echo their content (such as
  // JS handlers) should only be called once.
  $context = $contexts['argument_current_page_content_1'];
  if (!$context->processed) {
    if ($context->include_file) {
      require_once($context->include_file);
    }
    $context->data = call_user_func_array($context->callback, $context->arguments);
  }
  return $context->data;
}

/**
 * Callback to get a list of arguments provided by this task handler to the
 * variant's implementation.
 */
function panelator_site_template_get_arguments($task, $subtask_id) {
  return array(
    // URL is a regular string.  This allows all standard string-selection
    // criteria to apply.
    array(
      'keyword' => 'url',
      'identifier' => t('URL'),
      'id' => 1,
      'name' => 'string',
      'settings' => array(),
    ),

    // The "Current page content" context uses the original hook_menu
    // implementation to build the contents of the original page.
    array(
      'keyword' => 'current_page_content',
      'identifier' => t('Current page content'),
      'id' => 1,
      'name' => 'current_page_content',
      'settings' => array(),
    ),
  );
}

/**
 * Callback to get context placeholders provided by this handler.
 */
function panelator_site_template_get_contexts($task, $subtask_id) {
  return ctools_context_get_placeholders_from_argument(panelator_site_template_get_arguments($task, $subtask_id));
}

/**
 * Callback to enable/disable the page from the UI.
 */
function panelator_site_template_enable($cache, $status) {
  variable_set('panelator_site_template_disabled', $status);
}

/**
 * Check if panelator should be used for a given menu-router path.
 *
 * @param Array $router_paths.
 * An associative array keyed by the router path.  Set the value to TRUE to
 * enable a particular page.
 *
 * @return Array.
 */
function _panelator_use_panelator($items) {
  $use_on_admin_pages = variable_get('panelator_use_on_admin_pages', FALSE);
  $override_panel_pages = variable_get('panelator_override_other_panel_pages', FALSE);

  $router_paths = array_fill_keys(array_keys($items), FALSE);

  // Set default panelator behaviour for each path.
  foreach ($items as $path => $item) {

    // Don't handle these pages ever.
    if ($path == 'batch' ||
        $path == 'admin/build/block/list/js' ||
        $path == 'panels/ajax' ||
        $path == 'taxonomy/autocomplete' ||
        $path == 'user/autocomplete' ||
        $path == 'system/files' ||
        $path == 'logout' ||
        $path == 'rss.xml' ||
        $path == 'admin/reports/status/run-cron' ||
        strpos($path, 'admin/build/modules') === 0 ||
        strpos($path, 'admin/build/pages/%ctools_js') === 0 ||
        strpos($path, 'admin_menu/') === 0 ||
        strpos($path, 'ctools/') === 0
      ) {
      continue;
    }

    // Don't override other panel-pages.
    if (!$override_panel_pages && strpos($item['page callback'], 'page_manager_') === 0) {
      continue;
    }

    if (strpos($path, 'admin') === 0) {
      $router_paths[$path] = $use_on_admin_pages;
      continue;
    }

    // Default to being enabled for all other pages.
    $router_paths[$path] = TRUE;
  }

  // Allow a configuration option which lets other modules affect whether a
  // router-path uses panelator.
  // Invokes hook_panelator_enabled_router_paths_alter().
  if (variable_get('panelator_use_hook_panelator_enabled_router_paths_alter', FALSE)) {
    foreach (module_implements('panelator_enabled_router_paths_alter') as $module) {
      $callback = "{$module}_panelator_enabled_router_paths_alter";
      $callback($router_paths);
    }
  }
  return $router_paths;
}

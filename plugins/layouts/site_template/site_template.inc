<?php
/**
 * @file
 * Ctools layout plugin.
 */

// Plugin definition
$plugin = array(
  'title' => t('Site template'),
  'category' => t('Panelator'),
  'icon' => 'panelator_site_template.png',
  'theme' => 'panelator_site_template',
  'css' => 'panelator_site_template.css',
  'panels' => array(
    'left' => t('Left side'),
    'middle' => t('Middle column'),
    'right' => t('Right side')
  ),
);

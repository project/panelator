<?php

/**
 * @file
 * Menu callbacks to provide an admin interface for panelator's standard
 * settings.
 */

/**
 * Menu callback at admin/build/panels/settings/panelator
 */
function panelator_admin_settings(&$form_state) {
  $form = array();

  $form['pages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Control which pages use Panelator'),
  );
  $form['pages']['panelator_use_on_admin_pages'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use on admin pages'),
    '#default_value' => variable_get('panelator_use_on_admin_pages', FALSE),
  );
  $form['pages']['panelator_override_other_panel_pages'] = array(
    '#type' => 'checkbox',
    '#title' => t('Override other panel pages'),
    '#default_value' => variable_get('panelator_override_other_panel_pages', FALSE),
  );


  $form['template'] = array(
     '#type' => 'fieldset',
     '#title' => t('Panelator templates'),
  );
  $form['template']['panelator_provide_default_site_template'] = array(
    '#type' => 'checkbox',
    '#title' => t('Provide a default page variant for Panelator'),
    '#default_value' => variable_get('panelator_provide_default_site_template', TRUE),
  );

  return system_settings_form($form);
}

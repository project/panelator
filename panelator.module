<?php
/**
 * @file
 * Panelator allows every page to be displayed through panels.
 *
 * One panel to rule them all, one panel to find them, one panel to bring them
 * all and in the hook_menu bind them.
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function panelator_ctools_plugin_api($module, $api) {
  if ($module == 'page_manager' && $api == 'pages_default') {
    return array('version' => 1);
  }
}

/**
 * Implementation of hook_ctools_plugin_directory().
 */
function panelator_ctools_plugin_directory($module, $plugin) {
  if ($module == 'page_manager' || $module == 'ctools' || $module == 'panels' ) {
    return 'plugins/' . $plugin;
  }
}

/**
 * Implementation of hook_menu().
 */
function panelator_menu() {
  // Add an admin-config page.
  $items['admin/build/panels/settings/panelator'] = array(
    'title' => 'Panelator',
    'description' => 'Change whether panelator runs for admin pages and other panel pages',
    'access arguments' => array('administer page manager'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('panelator_admin_settings'),
    'file' => 'panelator.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implementation of hook_enable().
 */
function panelator_enable() {
  // Check that panelator hasn't been previously disabled.  If it's never been
  // configured, enable the default panels.
  if (variable_get('panelator_site_template_disabled', -1) == -1) {
    variable_set('panelator_site_template_disabled', FALSE);
    drupal_set_message(t('Panelator has been enabled.  You can now <a href="!url">review the panelator settings</a>.', array('!url' => url('admin/build/pages/edit/site_template'))));
  }

  // Panelator should usually be heavier than Page Manager.
  // This allows panelator to (optionally) override page manager pages.
  $panelator_weight = db_result(db_query("SELECT weight FROM {system} WHERE type='module' AND name='panelator'"));

  // Only change the weight of panelator if the weight is currently set to the
  // default weight of 0.
  if ($panelator_weight == 0) {
    $page_manager_weight = db_result(db_query("SELECT weight FROM {system} WHERE type='module' AND name='page_manager'"));
    db_query("UPDATE {system} SET weight = %d WHERE type='module' AND name='panelator'", ++$page_manager_weight);
  }
}
